--- /dev/null
+++ b/acpi_init.c
@@ -0,0 +1,712 @@
+// SPDX-License-Identifier: BSD-3-Clause OR GPL-2.0
+#include <acpi/acpi.h>
+
+#include <stdio.h>
+#include <sys/time.h>
+#include <sys/io.h>
+#include <stddef.h>
+#include <string.h>
+#include <ctype.h>
+#include <assert.h>
+#include <mach/vm_param.h>
+#include <errno.h>
+#include <sys/mman.h>
+#include <sys/types.h>
+#include <sys/stat.h>
+#include <fcntl.h>
+#include <unistd.h>
+#include <stdarg.h>
+#include <semaphore.h>
+#include <limits.h>
+
+#include <device/device.h>
+#include <hurd.h>
+#include <pciaccess.h>
+#include <hurd/irqhelp.h>
+
+#define ACPI_MAX_TABLES 128
+
+#define PCI_CFG1_START 0xcf8
+#define PCI_CFG1_END   0xcff
+
+#define LEGACY_ISA_IRQS 8
+#define PCI_IRQ_START 16
+
+extern acpi_status acpi_hw_legacy_sleep(u8 sleep_state);
+
+// Lets keep the ACPI tables in this module
+static struct acpi_table_desc initial_tables[ACPI_MAX_TABLES];
+
+static struct pci_device **pci_devices;
+static int numdevs = -1;
+
+struct slots {
+  uint8_t bus;
+  uint16_t dev;
+  uint16_t func;
+  acpi_handle handle;
+  struct slots *next;
+};
+
+static int
+pci_init(void)
+{
+  int i;
+  struct pci_device_iterator *dev_iter;
+  struct pci_device *pci_dev;
+  struct pci_device **tmp;
+
+  pci_system_init ();
+
+  numdevs = 0;
+  dev_iter = pci_slot_match_iterator_create (NULL);
+  while ((pci_dev = pci_device_next (dev_iter)) != NULL)
+    numdevs++;
+
+  pci_devices = malloc (sizeof(struct pci_device *));
+  if (pci_devices == NULL)
+    return errno;
+  tmp = pci_devices;
+
+  i = 0;
+  dev_iter = pci_slot_match_iterator_create (NULL);
+  while ((pci_dev = pci_device_next (dev_iter)) != NULL)
+    {
+      if ((tmp = realloc(pci_devices, (i + 1) * sizeof(*tmp))) == NULL)
+        {
+          free(pci_devices);
+          return errno;
+        }
+      pci_devices = tmp;
+      pci_device_probe(pci_dev);
+      pci_devices[i++] = pci_dev;
+    }
+  return 0;
+}
+
+struct pci_device *
+lookup_pci_dev(struct acpi_pci_id *pci_id)
+{
+  int i;
+
+  for (i = 0; i < numdevs; i++)
+    {
+      if ((pci_devices[i]->domain == pci_id->segment)
+       && (pci_devices[i]->bus == pci_id->bus)
+       && (pci_devices[i]->dev == pci_id->device)
+       && (pci_devices[i]->func == pci_id->function))
+        {
+          return pci_devices[i];
+        }
+    }
+    acpi_os_printf("Can't find pci dev %04x:%d:%02x.%d\n", pci_id->segment, pci_id->bus, pci_id->device, pci_id->function);
+    return NULL;
+}
+
+void
+acpi_ds_dump_method_stack(acpi_status status, ...)
+//    struct acpi_walk_state *walk_state, union acpi_parse_object *op)
+{
+  return;
+}
+
+void
+acpi_os_printf(const char *fmt, ...)
+{
+  va_list args;
+  va_start(args, fmt);
+  acpi_os_vprintf(fmt, args);
+  va_end(args);
+}
+
+void
+acpi_os_vprintf(const char *fmt, va_list args)
+{
+  vprintf(fmt, args);
+}
+
+acpi_status
+acpi_os_execute(acpi_execute_type type,
+    acpi_osd_exec_callback function, void *context)
+{
+  function(context);
+  return AE_OK;
+}
+
+acpi_physical_address
+acpi_os_get_root_pointer(void)
+{
+  acpi_physical_address pa;
+
+  acpi_find_root_pointer(&pa);
+  return pa;
+}
+
+acpi_status
+acpi_os_physical_table_override(struct acpi_table_header *existing_table,
+				acpi_physical_address *address,
+				u32 *table_length)
+{
+  *table_length = 0;
+  *address = 0;
+  return AE_OK;
+}
+
+acpi_status
+acpi_os_table_override(struct acpi_table_header *existing_table,
+		       struct acpi_table_header **new_table)
+{
+  if (!existing_table || !new_table)
+    return AE_BAD_PARAMETER;
+
+  *new_table = NULL;
+  return AE_OK;
+}
+
+void *
+acpi_os_map_memory(acpi_physical_address phys, acpi_size size)
+{
+  int err;
+  mach_port_t device_master;
+  device_t mem_device;
+  mach_port_t memobj;
+  vm_address_t vaddr = 0;
+  uint32_t pa_offset = phys % sysconf(_SC_PAGE_SIZE);
+  uint32_t pa_start = phys - pa_offset;
+
+  size = round_page(size + pa_offset);
+
+  err = get_privileged_ports (0, &device_master);
+  if (err)
+    {
+      acpi_os_printf("Cannot get device master port");
+      return (void *)-1;
+    }
+
+  err = device_open (device_master, 0, "mem", &mem_device);
+  mach_port_deallocate (mach_task_self (), device_master);
+  if (err)
+    {
+      acpi_os_printf("device_open(mem)");
+      return (void *)-1;
+    }
+
+  err =
+    device_map (mem_device, VM_PROT_READ | VM_PROT_WRITE, pa_start, size, &memobj, 0);
+  mach_port_deallocate (mach_task_self (), mem_device);
+
+  if (!err)
+    {
+      err = vm_map (mach_task_self (), &vaddr, size, 0, 1,
+		    memobj, 0, 0,
+		    VM_PROT_READ | VM_PROT_WRITE,
+		    VM_PROT_READ | VM_PROT_WRITE,
+		    VM_INHERIT_SHARE);
+      mach_port_deallocate (mach_task_self (), memobj);
+    }
+
+  if (err)
+    {
+      acpi_os_printf("Can't map memory 0x%llx\n", phys);
+      return (void *)-1;
+    }
+
+  return (void *)(vaddr + pa_offset);
+}
+
+void
+acpi_os_unmap_memory(void *virt, acpi_size size)
+{
+  void *freeme = (void *)trunc_page(virt);
+  if (!freeme)
+    {
+      acpi_os_printf("Nothing to unmap\n");
+      return;
+    }
+  munmap(freeme, size + size);
+}
+
+acpi_status
+acpi_os_read_memory(acpi_physical_address phys_addr, u64 *value, u32 width)
+{
+  void *virt_addr;
+  unsigned int size = width / 8;
+  acpi_status err = AE_BAD_ADDRESS;
+
+  if (!value)
+    return err;
+
+  virt_addr = acpi_os_map_memory (phys_addr, size);
+  if (!virt_addr)
+    return err;
+
+  if (!acpi_os_read_iomem (virt_addr, value, width))
+    err = AE_OK;
+
+  acpi_os_unmap_memory (virt_addr, size);
+
+  return err;
+}
+
+acpi_status
+acpi_os_write_memory(acpi_physical_address phys_addr, u64 value, u32 width)
+{
+  void *virt_addr;
+  unsigned int size = width / 8;
+  acpi_status err = AE_OK;
+
+  virt_addr = acpi_os_map_memory (phys_addr, size);
+  if (!virt_addr)
+    return AE_BAD_ADDRESS;
+
+  switch (width)
+    {
+      case 8:
+        *((volatile u8 *)virt_addr) = (u8)value;
+        break;
+      case 16:
+        *((volatile u16 *)virt_addr) = (u16)value;
+        break;
+      case 32:
+        *((volatile u32 *)virt_addr) = (u32)value;
+        break;
+      case 64:
+        *((volatile u64 *)virt_addr) = value;
+        break;
+      default:
+        acpi_os_printf("ACPI: Bad write memory width\n");
+        err = AE_BAD_ADDRESS;
+        break;
+    }
+
+  acpi_os_unmap_memory (virt_addr, size);
+
+  return err;
+}
+
+acpi_status
+acpi_os_initialize(void)
+{
+  /* Avoid giving ioperm to the PCI cfg registers
+   * since pci-arbiter controls these
+   */
+
+  /* 0-0xcf7 */
+  if (ioperm(0, PCI_CFG1_START, 1))
+    {
+      acpi_os_printf("acpi: EPERM on ioperm 1\n");
+      return AE_ERROR;
+    }
+
+  /* 0xd00-0xffff */
+  if (ioperm(PCI_CFG1_END+1, 0x10000 - (PCI_CFG1_END+1), 1))
+    {
+      acpi_os_printf("acpi: EPERM on ioperm 2\n");
+      return AE_ERROR;
+    }
+
+  if (pci_init())
+    {
+      acpi_os_printf("acpi pci_init: Error realloc\n");
+      return AE_ERROR;
+    }
+
+  if (irqhelp_init())
+    {
+      acpi_os_printf("acpi irqhelp_init: failed\n");
+      // FIXME: When libirqhelp is fixed: return AE_ERROR;
+    }
+  return AE_OK;
+}
+
+acpi_status
+acpi_os_terminate(void)
+{
+  free(pci_devices);
+  acpi_os_printf("Bye!\n");
+  return AE_OK;
+}
+
+acpi_status
+acpi_os_signal(u32 function, void *info)
+{
+  switch (function)
+    {
+      case ACPI_SIGNAL_FATAL:
+        acpi_os_printf("ACPI: Fatal opcode executed\n");
+        break;
+      case ACPI_SIGNAL_BREAKPOINT:
+        break;
+      default:
+        break;
+    }
+  return AE_OK;
+}
+
+acpi_status
+acpi_os_predefined_override(const struct acpi_predefined_names *init_val,
+                            acpi_string *new_val)
+{
+  if (!init_val || !new_val)
+    return AE_BAD_PARAMETER;
+
+  *new_val = 0;
+
+  return AE_OK;
+}
+
+void
+acpi_enter_sleep(int sleep_state)
+{
+  acpi_hw_legacy_sleep (sleep_state);
+}
+
+
+static acpi_status
+acpi_pci_link_count_all(struct acpi_resource *resource, void *context)
+{
+  int *count = (int *)context;
+
+  (*count)++;
+
+  return AE_OK;
+}
+
+static acpi_status
+acpi_pci_link_count_irq(struct acpi_resource *resource, void *context)
+{
+  int *count = (int *)context;
+
+  if (!resource->length)
+    return AE_OK;
+
+  switch (resource->type)
+    {
+      case ACPI_RESOURCE_TYPE_IRQ:
+        {
+          struct acpi_resource_irq *irq_resource = &resource->data.irq;
+          if (!irq_resource || !irq_resource->interrupt_count || !irq_resource->interrupts[0])
+            return AE_OK;
+          (*count)++;
+        }
+        break;
+      case ACPI_RESOURCE_TYPE_EXTENDED_IRQ:
+        {
+          struct acpi_resource_extended_irq *irq_resource = &resource->data.extended_irq;
+          if (!irq_resource || !irq_resource->interrupt_count || !irq_resource->interrupts[0])
+            return AE_OK;
+          (*count)++;
+        }
+        break;
+      default:
+        break;
+    }
+
+  return AE_OK;
+}
+
+static acpi_status
+acpi_pci_link_subset_last_irq(struct acpi_resource *resource, void *context)
+{
+  struct acpi_resource *res = (struct acpi_resource *)context;
+
+  switch (resource->type)
+    {
+      case ACPI_RESOURCE_TYPE_IRQ:
+        {
+          struct acpi_resource_irq *irq_resource = &resource->data.irq;
+          if (!irq_resource || !irq_resource->interrupt_count || !irq_resource->interrupts[0])
+            return AE_OK;
+          *res = *resource;
+          return AE_OK;
+        }
+        break;
+      case ACPI_RESOURCE_TYPE_EXTENDED_IRQ:
+        {
+          struct acpi_resource_extended_irq *irq_resource = &resource->data.extended_irq;
+          if (!irq_resource || !irq_resource->interrupt_count || !irq_resource->interrupts[0])
+            return AE_OK;
+          *res = *resource;
+          return AE_OK;
+        }
+        break;
+      default:
+        break;
+    }
+
+  return AE_OK;
+}
+
+static acpi_status
+acpi_pci_link_get_last_valid_irq(struct acpi_resource *resource, void *context)
+{
+  int *irq = (int *)context;
+
+  if (!resource->length)
+    {
+      acpi_os_printf("Empty resource\n");
+      return AE_OK;
+    }
+
+  switch (resource->type)
+    {
+      case ACPI_RESOURCE_TYPE_IRQ:
+        {
+          struct acpi_resource_irq *irq_resource = &resource->data.irq;
+          if (!irq_resource || !irq_resource->interrupt_count || !irq_resource->interrupts[0])
+            {
+              // no IRQ# bits set generally w/ _STA disabled
+              acpi_os_printf("Empty _CRS IRQ resource\n");
+              return AE_OK;
+            }
+          *irq = irq_resource->interrupts[0];
+        }
+        break;
+      case ACPI_RESOURCE_TYPE_EXTENDED_IRQ:
+        {
+          struct acpi_resource_extended_irq *irq_resource = &resource->data.extended_irq;
+          if (!irq_resource || !irq_resource->interrupt_count || !irq_resource->interrupts[0])
+            {
+              // extended IRQ descriptors should return at least 1 IRQ
+              acpi_os_printf("Empty _CRS EXT IRQ resource\n");
+              return AE_OK;
+            }
+          *irq = irq_resource->interrupts[0];
+        }
+        break;
+      case ACPI_RESOURCE_TYPE_END_TAG:
+        return AE_OK;
+        break;
+      default:
+        acpi_os_printf("Not IRQ resource\n");
+        return AE_OK;
+    }
+
+  acpi_os_printf("Found IRQ resource %d\n", *irq);
+  return AE_OK;
+}
+
+int
+acpi_get_irq_number(uint16_t bus, uint16_t dev, uint16_t func)
+{
+  struct acpi_buffer buffer = { ACPI_ALLOCATE_BUFFER, NULL };
+  struct acpi_buffer srs_buffer = { ACPI_ALLOCATE_BUFFER, NULL };
+  struct acpi_pci_routing_table *entry;
+  struct acpi_resource *res;
+  uint8_t *buf;
+  int srs_count;
+
+  acpi_handle handle = ACPI_ROOT_OBJECT;
+  acpi_handle parent_handle = NULL;
+  acpi_handle prt = NULL;
+  acpi_handle lnk = NULL;
+  acpi_status err = AE_OK;
+  u16 prt_dev, prt_func;
+
+  err = acpi_get_handle(handle, "\\_SB.PCI0", &parent_handle);
+  if (ACPI_FAILURE(err))
+    return -EIO;
+
+  err = acpi_get_handle(parent_handle, METHOD_NAME__PRT, &prt);
+  if (ACPI_FAILURE(err))
+    return -EIO;
+
+  err = acpi_evaluate_object(prt, NULL, NULL, &buffer);
+  if (ACPI_FAILURE(err))
+    return -EIO;
+
+  err = acpi_get_irq_routing_table(parent_handle, &buffer);
+  if (ACPI_FAILURE(err))
+    return -EIO;
+
+  entry = ACPI_CAST_PTR(struct acpi_pci_routing_table, ACPI_CAST_PTR(u8, buffer.pointer));
+  while (entry && (entry->length > 0))
+    {
+      /* Already applies to the bus of the device */
+      prt_dev = (entry->address >> 16) & 0xffff;
+      prt_func = entry->address & 0xffff;
+      if ((prt_dev == dev) && (prt_func == 0xffff))
+        {
+          if (entry->source[0])
+            {
+              int crs_count = 0;
+              int prs_count_irq = 0;
+              int prs_count_all = 0;
+              int irq = -1;
+
+              /* Dynamic:
+               * - source field specifies PCI interrupt LNK
+               * - source_index specifies which resource descriptor in
+               *   resource template of LNK to allocate this interrupt
+               */
+              err = acpi_get_handle(ACPI_ROOT_OBJECT, entry->source, &lnk);
+              if (ACPI_FAILURE(err))
+                return -EIO;
+
+              if (!lnk)
+                {
+                  acpi_os_printf("Error invalid lnk\n");
+                  return -EIO;
+                }
+
+              err = acpi_walk_resources(lnk, "_CRS", acpi_pci_link_count_irq, &crs_count);
+              if (ACPI_FAILURE(err) || !crs_count)
+                acpi_os_printf("Warning: missing _CRS\n");
+              acpi_walk_resources(lnk, "_PRS", acpi_pci_link_count_all, &prs_count_all);
+              err = acpi_walk_resources(lnk, "_PRS", acpi_pci_link_count_irq, &prs_count_irq);
+              if (ACPI_FAILURE(err))
+                {
+                  if (crs_count == 0)
+                    {
+                      acpi_os_printf("Error: missing _PRS when needed\n");
+                      return -EIO;
+                    }
+                }
+              if (crs_count > 0)
+                {
+                  irq = 0;
+                  err = acpi_walk_resources(lnk, "_CRS", acpi_pci_link_get_last_valid_irq, &irq);
+                  if (ACPI_FAILURE(err) || !irq)
+                    {
+                      acpi_os_printf("Error walk_resources _CRS\n");
+                      return -EIO;
+                    }
+
+                  err = acpi_get_current_resources(lnk, &srs_buffer);
+                  if (ACPI_FAILURE(err))
+                    {
+                      acpi_os_printf("Error getting _CRS\n");
+                      return -EIO;
+                    }
+
+                  err = acpi_set_current_resources(lnk, &srs_buffer);
+                  if (ACPI_FAILURE(err))
+                    {
+                      acpi_os_printf("Error setting _SRS\n");
+                      return -EIO;
+                    }
+                  irq += (irq > LEGACY_ISA_IRQS) ? 0 : PCI_IRQ_START;
+                  acpi_os_printf("Final irq %d\n", irq);
+                  return irq;
+                }
+              else if (prs_count_irq > 0)
+                {
+                  irq = 0;
+                  err = acpi_walk_resources(lnk, "_PRS", acpi_pci_link_get_last_valid_irq, &irq);
+                  if (ACPI_FAILURE(err) || !irq)
+                    {
+                      acpi_os_printf("Error walk_resources _PRS\n");
+                      return -EIO;
+                    }
+
+                  if (irq > 16)
+                    {
+                      acpi_os_printf("Error IRQ > 16, not implemented\n");
+                      return -EIO;
+                    }
+
+                  /* Found a possible irq, but need to call _SRS to set it */
+
+                  /* IRQ resource + end tagged resource */
+                  srs_count = 2;
+
+                  res = (struct acpi_resource *)calloc(1, srs_count * sizeof(struct acpi_resource));
+
+                  err = acpi_walk_resources(lnk, "_PRS", acpi_pci_link_subset_last_irq, res);
+                  if (err)
+                    {
+                      acpi_os_printf("Error _PRS cannot be read\n");
+                      free(res);
+                      return -EIO;
+                    }
+
+                  /* Patch the _PRS to use for _SRS */
+                  srs_buffer.length = sizeof(struct acpi_resource) + 1;
+                  srs_buffer.pointer = res;
+
+                  switch (res->type)
+                    {
+                      case ACPI_RESOURCE_TYPE_IRQ:
+                        res->data.irq.interrupt_count = 1;
+                        res->data.irq.interrupts[0] = irq;
+                        break;
+                      case ACPI_RESOURCE_TYPE_EXTENDED_IRQ:
+                        res->data.extended_irq.interrupt_count = 1;
+                        res->data.extended_irq.interrupts[0] = irq;
+                        break;
+                    }
+
+                  res[1].type = ACPI_RESOURCE_TYPE_END_TAG;
+                  res[1].length = sizeof(struct acpi_resource);
+
+                  err = acpi_set_current_resources(lnk, &srs_buffer);
+                  if (ACPI_FAILURE(err))
+                    {
+                      acpi_os_printf("Error setting _SRS\n");
+                      free(res);
+                      return -EIO;
+                    }
+                  free(res);
+                  irq += (irq > LEGACY_ISA_IRQS) ? 0 : PCI_IRQ_START;
+                  acpi_os_printf("Final irq %d\n", irq);
+                  return irq;
+                }
+              else
+                {
+                  acpi_os_printf("Error: _CRS has empty IRQ resources\n");
+                  return -EIO;
+                }
+            }
+          else
+            {
+              /* Static:
+               * - source field is zero
+               * - source_index specifies IRQ value hardwired to
+               *   interrupt inputs on controller
+               */
+              return entry->source_index;
+            }
+        }
+
+      entry = ACPI_ADD_PTR(struct acpi_pci_routing_table, entry, entry->length);
+    }
+
+  return -EIO;
+}
+
+void acpi_init(void)
+{
+  acpi_status err;
+
+  // Hack to increase verbosity except parsing AML
+  //acpi_dbg_level = (ACPI_LV_ALL) & ~(ACPI_LV_FUNCTIONS);
+
+  err = acpi_initialize_tables (initial_tables, ACPI_MAX_TABLES, 0);
+  if (ACPI_FAILURE (err))
+    goto die;
+
+  err = acpi_initialize_subsystem ();
+  if (ACPI_FAILURE (err))
+    goto die;
+
+  err = acpi_reallocate_root_table ();
+  if (ACPI_FAILURE (err))
+    goto die;
+
+  err = acpi_load_tables ();
+  if (ACPI_FAILURE (err))
+    goto die;
+
+  err = acpi_enable_subsystem (ACPI_FULL_INITIALIZATION);
+  if (ACPI_FAILURE (err))
+    goto die;
+
+  err = acpi_initialize_objects (ACPI_FULL_INITIALIZATION);
+  if (ACPI_FAILURE (err))
+    goto die;
+
+  acpi_os_printf("PASS!\n");
+  return;
+die:
+  acpi_os_printf("OUCH!\n");
+}
--- /dev/null
+++ b/include/acpi/acpi_init.h
@@ -0,0 +1,11 @@
+// SPDX-License-Identifier: BSD-3-Clause OR GPL-2.0
+#ifndef ACPI_INIT_H_
+#define ACPI_INIT_H_
+
+#include <stdint.h>
+
+void acpi_enter_sleep(int sleep_state);
+int acpi_get_irq_number(uint16_t bus, uint16_t dev, uint16_t func);
+void acpi_init(void);
+
+#endif
